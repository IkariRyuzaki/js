if ARGV.length < 1 
	puts "No input files."
	exit true
end

begin
	if File.file?(ARGV[1])
		File.open(ARGV[1]).each do |line|
			puts "#{line}"
		end
	else
		puts "File #{ARGV[1]}, does not exist."
	end
rescue
	puts "No write permissions, please run as root/administrator.";
end
