# JS
Please do not confuse JS with Javascript

# Getting started!
To get started create a file with some text in it, like this

- [ ] [Hi guys!]
- [ ] [I love JS]
- [ ] [Better than web assembly!]

Then run the interpreter on it with this command
- [ ] [ruby Interpreter.rb FileName]

In this case FileName is the name of the file we just created with the text about how we love Fortan

This will output the following

- [ ] [Hi guys!]
- [ ] [I love JS]
- [ ] [Better than web assembly!]

If you wish to learn more, please feel free to look around in the source code.
